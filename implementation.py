import requests
import datetime
import pandas as pd

class LicenseDrivers:
    def collect_data_from_api(self, api_url):
        response = requests.get(api_url)
        if response.status_code == 200:
            return response.json()
        else:
            print("Can't collect data from API because the status code is " + str(response.status_code))
            return None


    def list_of_suspended_licenses(self, data):
        suspended_licenses = [license for license in data if license.get("suspendat")]
        return suspended_licenses


    def extract_valid_licenses(self, data):
        today = datetime.datetime.now()
        valid_licenses = [license for license in data if datetime.datetime.strptime(license["dataDeExpirare"], "%d/%m/%Y") >= today]
        return valid_licenses


    def find_licenses_based_on_category(self, data):
        license_counts = {}
        for license in data:
            category = license["categorie"]
            if category in license_counts:
                license_counts[category] += 1
            else:
                license_counts[category] = 1
        formatted_counts = [{"Category": category, "Count": count} for category, count in license_counts.items()]
        return formatted_counts


    def perform_operation(self, data, operation_id):
        if operation_id == 1:
            result = self.list_of_suspended_licenses(data)
            print("Suspended Licenses:")
            for item in result:
                print(item)
            self.export_to_excel(result, "suspended_licenses.xlsx")

        elif operation_id == 2:
            result = self.extract_valid_licenses(data)
            print("Valid Licenses:")
            for item in result:
                print(item)
            self.export_to_excel(result, "valid_licenses.xlsx")

        elif operation_id == 3:
            result = self.find_licenses_based_on_category(data)
            print("License Counts by Category:")
            for item in result:
                print(item)
            self.export_to_excel(result, "license_counts_by_category.xlsx")

        else:
            print("Invalid operation ID.")


    def export_to_excel(self, data, filename):
        df = pd.DataFrame(data)
        df.to_excel(filename, index=False)
        print("Data exported to " + filename + " successfully.")


api_url = "http://localhost:8080/drivers-licenses/list"

authority = LicenseDrivers()

data = authority.collect_data_from_api(api_url)

if data:
    while True:
        print("Operations:")
        print("1. List suspended licenses")
        print("2. Extract valid licenses")
        print("3. Find license counts by category")
        print("4. Exit")

        operation_id = int(input("Enter operation ID: "))

        if operation_id == 4:
            break

        authority.perform_operation(data, operation_id)
else:
    print("Failed to fetch data from API.")