Commands used:

docker run -d -p 8080:30000 yondermakers/yonder-devops-tech-assessment:latest
-> to run the docker container
apt-get update 
apt-get install curl -> for accessing the endpoints
apt-get install jq->for json data
curl -vvv localhost:8080/ -> to list the questions
curl -vvv localhost:8080/drivers-licenses/list | jq -S . -> to list the returned list in json format
apt-get install dnsutils -> for using nslookup

For implementation:
apt-get install nano -> using nano for creating python script
apt-get install pip ->for installing python libraries
pip install requests -> for installing request python library
pip install pandas -> for installing pandas python library
pip install openpyxl -> is required for pandas for writing excel files
cd /usr/lib/python3/dist-packages/pip -> to access the path for pip
nano implement.py -> to create the python script in the /usr/lib/python3/dist-packages/pip path
python3 implement.py -> to launch the script in the /usr/lib/python3/dist-packages/pip path


nslookup www.tss-yonder.com
Server:         192.168.65.7
Address:        192.168.65.7#53

Non-authoritative answer:
Name:   www.tss-yonder.com
Address: 104.26.1.62
Name:   www.tss-yonder.com
Address: 172.67.73.177
Name:   www.tss-yonder.com
Address: 104.26.0.62
Name:   www.tss-yonder.com
Address: 2606:4700:20::681a:13e
Name:   www.tss-yonder.com
Address: 2606:4700:20::ac43:49b1
Name:   www.tss-yonder.com
Address: 2606:4700:20::681a:3e

Questions:

1) Two data structures:
-array => Arrays can be used to store collections of data of the same type (for example a list of product). Can be used in sorting algorithms
-stack => LIFO/FILO. Local variables are managed in stack (when a function is called, the local variables are pushed onto the stack, and when the function returns, they are popped off the stack)

2) Using this command nslookup www.tss-yonder.com to retrieve the IP address/addresses for www.tss-yonder.com nslookup www.tss-yonder.com
DNS servers maintain a distributed database of domain names and their corresponding IP addresses, allowing browsers to quickly find the correct IP address for a given domain.

3) TCP(Transmission Control Protocol)-used to establish a connection between your compute and the web server hosting the website that you want to access
                                     -data packets are delivered safe, without error and in order (high reliability)=> best choice for file transfer 
   UDP(User Datagrm Protocol)-reduce latency and transfer a lot of small packets over the network =>best choice for streaming or real-time communication  
                             -packet loss
4) To host an application:
->choosing a Cloud Service Provider (CSP). I will choose AWS-EC2)
->can be used virtual machines or containers to deploy the app. I will use Docker to work with containers instead of VMs
->if you have a database, need to choose a DB service and setup the DB. I will choose MySql for exemple.
->deploy the application. I will use Ansible (for automation), Jenkins for CI/CD pipelines (automated deploy and optimizated scalability) + Kubernetes (orchestration) 
->monitoring. I will use Zabbix, Grafana, Elastic Search and AppDynamics

5) I will try to limit access for my web application only for my friends to working for fixing the bug;
Once the bugs are resolved and the application was tested, I will remove the restrictions that was applied.
-my friends can work to fixing the bugs by using the repo created for my web application and share together the code
-or make a docker image for this application and give to my friends for debugging 

6) Key encryption (a strategy which can be used is end-to-end encryption)
-> using a public key for cripting messages(this key is exchanged between users)
-> using a private key for decripting the messages (unique for each user)
Can be used an encryption algorithm like RSA

7) Cookies are used to maintain data from the active session, for example, how shopping carts are saved if you exit the browser, the items remain saved, or for example, if you set your browser to dark mode
name: PHPSESSID ->This cookie is commonly used to track the user's session on a website. It stores a unique identifier for the session
value: 0mnekiuiqn1nv9vkdfbkgf01s7
name:icwp-wpsf-notbot
value: 1710440108z5865d7a73b593e5d9b9bc1978d6daf38c9588c23

8) Multithreading in java for example;
starting from a thread of execution; multithreading -> multiple threads of execution; 
Possible states: threads running, waiting
Multiprocessing module in python to create child processes
Possible states: running, ready, blocked

9) Using the command: ps aux to give all processes and correspondent PID -> quick search using the same command but like ps aux | grep name_of_application to receive the corresponding proccesses for the app; 
Searching in logs in /var/log/your_apps and start debugging (Reviewing the application's logs to identify any error messages or exceptions that occurred)

10) MySQL -> vault secret to encrypt the password for each user
